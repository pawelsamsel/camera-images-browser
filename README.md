# Camera Image Browser 

Simple image browser from a selected folder that allows to delete jpg or jpg+raw with a single click for quick removal of unwanted or failed shots.

### Keyboard shortcuts

 - Open folder - 0 (zero)
 - Next Image - Right arrow or 6
 - Prev Image - Left arrow or 4
 - Delete only RAW - 1
 - Delete both JPG and RAW - 2
 - Delete only JPG - 3
 
 
 ### To run installer project - add-on to Visual Studio is needed. It can be downloaded from:

 - VS2015 https://marketplace.visualstudio.com/items?itemName=VisualStudioProductTeam.MicrosoftVisualStudio2015InstallerProjects
 - VS2017 https://marketplace.visualstudio.com/items?itemName=VisualStudioProductTeam.MicrosoftVisualStudio2017InstallerProjects