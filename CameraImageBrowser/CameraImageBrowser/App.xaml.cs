﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace CameraImageBrowser
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private MainWindow mainWindow;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (e.Args.Length > 0 )
            {
                // check if first argument is a path to file (from option open file with ...)
                // if argument is a file - check if :\ exists and if file exists, then extract folder and pass to AppController

                //System.IO.StreamWriter file = new System.IO.StreamWriter("e:\\test.txt", true);
                //foreach(var arg in e.Args)
                //{
                //    file.WriteLine(arg);
                //}
                //file.Close();
            }
            mainWindow = new MainWindow();
            mainWindow.ShowDialog();
            this.Shutdown();
        }

    }

    
}
