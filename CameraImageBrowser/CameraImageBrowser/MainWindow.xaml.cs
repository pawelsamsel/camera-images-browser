﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using CameraImageBrowser.Data;


namespace CameraImageBrowser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ImagesDirectory imagesDirectory;

        public MainWindow()
        {
            InitializeComponent();
            imagesDirectory = ImagesDirectory.Instance;
            updateUI();
        }


        private void browseFolder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            openDirectory();
        }

        private void openDirectory()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    imagesDirectory.OpenDirectory(dialog.SelectedPath);
                    updateUI();
                }
            }
        }

        private void showNextImageItem(object sender, MouseButtonEventArgs e)
        {
            showNextImage();
        }

        private void showNextImage()
        {
            imagesDirectory.NextImageItem();
            updateUI();
        }

        private void showPrevImageItem(object sender, MouseButtonEventArgs e)
        {
            showPrevImage();
        }

        private void showPrevImage()
        {
            imagesDirectory.PrevImageItem();
            updateUI();
        }

        private void deleteJpgFile(object sender, MouseButtonEventArgs e)
        {
            deleteJpg();
        }

        private void deleteJpg()
        {
            imagePreview.Source = null;
            imagesDirectory.DeleteCurrentJpgFile();
            updateUI();
        }

        private void deleteRawFile(object sender, MouseButtonEventArgs e)
        {
            deleteRaw();
        }

        private void deleteRaw()
        {
            imagePreview.Source = null;
            imagesDirectory.DeleteCurrentRawFile();
            updateUI();
        }

        private void deleteJpgAndRawFiles(object sender, MouseButtonEventArgs e)
        {
            deleteJpgAndRaw();
        }

        private void deleteJpgAndRaw()
        {
            imagePreview.Source = null;
            imagesDirectory.DeleteCurrentJpgAndRawFiles();
            updateUI();
        }

        private void updateUI()
        {
            currentPositionStatus.Content = imagesDirectory.ImageItemsCount > 0 ? (imagesDirectory.CurrentImageItemIdx + 1) + "/" + imagesDirectory.ImageItemsCount : "no supported images";
            currentImageName.Content = imagesDirectory.CurrentImageItem != null ? imagesDirectory.CurrentImageItem.name : "no image";
            currentImageFilesTypes.Content = getImageItemFileTypes(imagesDirectory.CurrentImageItem);

            enableImageButton(prevImage, imagesDirectory.ImageItemsCount > 0 && imagesDirectory.CurrentImageItemIdx > 0);
            enableImageButton(nextImage, imagesDirectory.ImageItemsCount > 0 && imagesDirectory.CurrentImageItemIdx < imagesDirectory.ImageItemsCount - 1);

            enableImageButton(trashJpg, imagesDirectory.CurrentImageItem != null && imagesDirectory.CurrentImageItem.jpgFilePath != null);
            enableImageButton(trashRaw, imagesDirectory.CurrentImageItem != null && imagesDirectory.CurrentImageItem.rawFilePath != null);
            enableImageButton(trashBoth, trashJpg.IsEnabled && trashRaw.IsEnabled);

            imagePreview.Source = getImagePreviewSource(imagesDirectory.CurrentImageItem);
        }

        private BitmapImage getImagePreviewSource(ImageItem imageItem)
        {
            if (imageItem != null && imageItem.jpgFilePath != null)
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = new Uri(imageItem.jpgFilePath);
                image.EndInit();
                return image;
            }
            else
            {
                return null;
            }
        }

        private string getImageItemFileTypes(ImageItem imageItem)
        {
            if(imageItem != null)
            {
                List<string> types = new List<string>();
                if (imageItem.jpgFilePath != null)
                {
                    types.Add("JPG");
                }
                if (imageItem.rawFilePath != null)
                {
                    types.Add("RAW");
                }
                return String.Join(" + ", types.ToArray());
            } 
            else
            {
                return "";
            }
        }

        private double getImageOpacity(Image imageElement)
        {
            return imageElement.IsEnabled ? 0.9 : 0.3;
        }

        private void enableImageButton(Image imageButton, bool enable)
        {
            imageButton.IsEnabled = enable;
            imageButton.Opacity = getImageOpacity(imageButton);
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.NumPad0:
                case Key.D0:
                    openDirectory();
                    break;
                case Key.NumPad4:
                case Key.D4:
                case Key.Left:
                    showPrevImage();
                    break;
                case Key.NumPad6:
                case Key.D6:
                case Key.Right:
                    showNextImage();
                    break;
                case Key.NumPad1:
                case Key.D1:
                    deleteRaw();
                    break;
                case Key.NumPad2:
                case Key.D2:
                    deleteJpgAndRaw();
                    break;
                case Key.NumPad3:
                case Key.D3:
                    deleteJpg();
                    break;
                default:
                    break;
            }
        }
    }
}
