﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CameraImageBrowser.Data
{
    class ImageItem
    {
        public string jpgFilePath;
        public string rawFilePath;
        public string name;
    }
}
