﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace CameraImageBrowser.Data
{
    class ImagesDirectory
    {
        #region Properties

        private static ImagesDirectory instance;

        public static ImagesDirectory Instance {
            get
            {
                if(instance == null)
                {
                    instance = new ImagesDirectory();
                }
                return instance;
            }
        }

        public ImageItem CurrentImageItem
        {
            get { return currentImageItem; }
        }

        public int CurrentImageItemIdx
        {
            get { return currentImageItem != null && imageItems != null ? imageItems.IndexOf(CurrentImageItem) : -1; }
        }

        public int ImageItemsCount
        {
            get { return imageItems != null ? imageItems.Count : 0; }
        }

        private string currentDirectory = "";
        private List<ImageItem> imageItems;
        private ImageItem currentImageItem;

        #endregion


        public void OpenDirectory(string directoryPath)
        {
            if (Directory.Exists(directoryPath))
            {
                currentDirectory = directoryPath;
                RefreshImageItems();
                currentImageItem = imageItems.Count > 0 ? imageItems[0] : null;
            }
        }

        public void RefreshImageItems()
        {
            imageItems = getCurrentImagesList(currentDirectory);
        }

        public void NextImageItem()
        {
            if(CurrentImageItemIdx < imageItems.Count-1)
            {
                currentImageItem = imageItems[CurrentImageItemIdx + 1];
            }
        }

        public void PrevImageItem()
        {
            if (CurrentImageItemIdx > 0)
            {
                currentImageItem = imageItems[CurrentImageItemIdx - 1];
            }
        }


        private List<ImageItem> getCurrentImagesList(string directory)
        {
            if (Directory.Exists(directory))
            {
                var imageItems = new Dictionary<string, ImageItem>();

                foreach(string filePath in Directory.GetFiles(directory))
                {

                    string extension = getSanitizedExtenision(filePath);
                    if (getSupportedImagesExtenisions().Contains<string>(extension))
                    {
                        updateImageItem(filePath, imageItems);
                    }
                    
                }
                return imageItems.Values.ToList<ImageItem>();
            }
            else
            {
                return new List<ImageItem>();
            }
        }

        private ImageItem updateImageItem(string filePath, Dictionary<string, ImageItem> imageItems)
        {
            string filename = Path.GetFileNameWithoutExtension(filePath);
            string extension = getSanitizedExtenision(filePath);

            ImageItem imageItem = getImageItemByName(filename, imageItems);

            if (getSupportedJpgExtensions().Contains<string>(extension))
            {
                imageItem.jpgFilePath = filePath;
            }
            else if (getSupportedRawExtensions().Contains<string>(extension))
            {
                imageItem.rawFilePath = filePath;
            }

            return imageItem;
        }

        private string getSanitizedExtenision(string filePath)
        {
            return Path.GetExtension(filePath).ToLowerInvariant().Replace(".", "");
        }

        private ImageItem getImageItemByName(string name, Dictionary<string, ImageItem> images)
        {
            ImageItem item;
            images.TryGetValue(name, out item);
            if(item == null)
            {
                item = new ImageItem();
                item.name = name;
                images.Add(name, item);
            }
            return item;
        }

        static public string[] getSupportedImagesExtenisions()
        {
            List<string> list = new List<string>(getSupportedJpgExtensions());
            list.AddRange(getSupportedRawExtensions());
            return list.ToArray();
        }

        static public string[] getSupportedJpgExtensions()
        {
            return new string[] { "jpg", "jpeg" };
        }

        static public string[] getSupportedRawExtensions()
        {
            return new string[] { "nef" };
        }

        public void DeleteCurrentJpgAndRawFiles()
        {
            DeleteCurrentJpgFile();
            DeleteCurrentRawFile();
        }

        public void DeleteCurrentJpgFile()
        {
            if (currentImageItem.jpgFilePath != null)
            {
                copyToDeleteFolder(currentImageItem);
                deleteImage(currentImageItem.jpgFilePath);
                currentImageItem.jpgFilePath = null;
                tryDeleteImageItem(currentImageItem);
            }
        }

        public void DeleteCurrentRawFile()
        {
            if(currentImageItem.rawFilePath != null)
            {
                copyToDeleteFolder(currentImageItem);
                deleteImage(currentImageItem.rawFilePath);
                currentImageItem.rawFilePath = null;
                tryDeleteImageItem(currentImageItem);
            }
        }

        private void tryDeleteImageItem(ImageItem imageItem)
        {
            if (imageItem.jpgFilePath == null && imageItem.rawFilePath == null)
            {
                int currentItemIdx = CurrentImageItemIdx;
                currentItemIdx = (currentItemIdx == imageItems.Count - 1) ? imageItems.Count - 2 : currentItemIdx;
                imageItems.Remove(imageItem);
                currentImageItem = imageItems[currentItemIdx];
            }
        }

        private void deleteImage (string filePath)
        {
            File.Delete(filePath);
        }

        private void copyToDeleteFolder(ImageItem imageItem)
        {
            if (imageItem.jpgFilePath != null )
            {
                copyFileToDeleteFolder(imageItem.jpgFilePath);
            }

            if (imageItem.rawFilePath != null)
            {
                copyFileToDeleteFolder(imageItem.rawFilePath);
            }
        }

        private void copyFileToDeleteFolder(string filePath)
        {
            checkDeleteDirectory();
            string destinationFilePath = getDeleteFolderPath() + Path.DirectorySeparatorChar + Path.GetFileName(filePath);
            if (!File.Exists(destinationFilePath))
            {
                File.Copy(filePath, destinationFilePath);
            }
        }

        private void checkDeleteDirectory()
        {

            string deleteDirectoryPath = getDeleteFolderPath();
            if (!Directory.Exists(deleteDirectoryPath))
            {
                Directory.CreateDirectory(deleteDirectoryPath);
            }
        }

        private string getDeleteFolderPath()
        {
            string deleteFolderName = "delete-me";
            return currentDirectory + Path.DirectorySeparatorChar + deleteFolderName;
        }
    }


}
