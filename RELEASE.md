# Camera Image Browser Release Notes

### Camera Image Browser v1.0.0

  - Added deleting only JPG files
  - Added deleting only RAW files
  - Added deleting both JPG and RAW files
  - Added folder selection window
  - Added UI
  - Added navigation over images list
  - Added keyboard control